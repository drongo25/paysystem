<?php

namespace App\Contracts\ConfirmationMethods;

interface ConfirmationMethodBuildInterface
{
    public static function Build(string $sender): ConfirmationMethodInterface;
}
