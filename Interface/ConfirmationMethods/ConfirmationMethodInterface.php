<?php

namespace App\Contracts\ConfirmationMethods;

interface ConfirmationMethodInterface
{
    public function sendCode(object $user): bool;

    public function getErrors(): array;
}
