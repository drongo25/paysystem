<?php

namespace App\Controllers\ConfirmationMethods;

use App\Services\ConfirmationMethods\ConfirmationMethodBuild;
use App\Helpers\ConfirmationMethods\ConfirmationHelper;

class UserSettingsController
{

    /**
     * @param object $request
     * @param object $user
     * @return array
     * @throws \Exception
     */
    public function sendCode(object $request, object $user): array
    {
        $sender = ConfirmationMethodBuild::Build($request->type);
        $result = ['success' => $sender->sendCode($user)];
        $errors = $sender->getErrors();

        if (!empty($errors)) {
            http_send_status(520);
            $result['errors'] = $errors;
        }

        return $result;
    }

    /**
     * @param object $request
     * @param object $user
     * @return array
     */
    public function update(object $request, object $user): array
    {
        $valid = ConfirmationHelper::validate($user->id, $request->code);

        if ($valid !== true) {
            return [
                'success' => false,
                'error' => 'Not valid code'
            ];
        }

        $userSettings = $user->getUserSettings()
            ->where('user_id', $user->id)
            ->where('name', $request->name)
            ->get();

        if (empty($userSettings)) {
            return [
                'success' => false,
                'error' => 'Not valid setting'
            ];
        }

        ConfirmationHelper::unsetCode($user->id);

        $userSettings->update(['value' => $request->value]);

        return [
            'success' => true,
        ];
    }
}

