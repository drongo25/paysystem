<?php
namespace App\Services\ConfirmationMethods;


use App\Contracts\ConfirmationMethods\ConfirmationMethodInterface;

class ConfirmationMethodService implements ConfirmationMethodInterface
{
    /**
     * @param object $user
     * @return bool
     */
    public function sendCode(object $user): bool
    {
        // TODO: Implement sendCode() method.
        return true;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        // TODO: Implement getErrors() method.

        return [
            'error' => true,
        ];
    }
}
