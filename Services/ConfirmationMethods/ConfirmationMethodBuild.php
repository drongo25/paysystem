<?php

namespace App\Services\ConfirmationMethods;

use App\Contracts\ConfirmationMethods\ConfirmationMethodInterface;
use App\Contracts\ConfirmationMethods\ConfirmationMethodBuildInterface;
use App\Services\ConfirmationMethods\ConfirmationByEmail;
use App\Services\ConfirmationMethods\ConfirmationBySms;
use App\Services\ConfirmationMethods\ConfirmationByTelegram;

class ConfirmationMethodBuild implements ConfirmationMethodBuildInterface
{
    /**
     * @param string $sender
     * @return ConfirmationMethodInterface
     * @throws \Exception
     */
    public static function Build(string $sender): ConfirmationMethodInterface
    {
        return match ($sender) {
            'email' => new ConfirmationByEmail(),
            'sms' => new ConfirmationBySms(),
            'telegram' => new ConfirmationByTelegram(),
            default => throw new \Exception('Unsupported sender'),
        };
    }
}
