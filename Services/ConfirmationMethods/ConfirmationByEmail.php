<?php

namespace App\Services\ConfirmationMethods;

use App\Contracts\ConfirmationMethods\ConfirmationMethodInterface;
use App\Helpers\ConfirmationMethods\ConfirmationHelper;

class ConfirmationByEmail implements ConfirmationMethodInterface
{
    private array $errors = [];
    /**
     * @param object $user
     * @return bool
     */
    public function sendCode(object $user): bool
    {
        $code = ConfirmationHelper::generate($user->id);
        // логика получения данных юзера и генерация сообщения
        return true;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
